
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.awt.FlowLayout;
import javax.swing.*;

import java.util.List;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
//import javax.swing.ImageIcon;
//import javax.swing.JButton;
//import javax.swing.JFrame;
//import javax.swing.JLabel;
//import javax.swing.SwingConstants;
//import javax.swing.border.BevelBorder;




public class Burger_Main {
	
	//이게 머지???
	public static Burger_Main burgerMain;
	protected JFrame frame;
	
	protected JLabel answer;
	//images들을 배열 하나에 몰아서 담는다. 
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try
				{
					burgerMain = new InputButtonController();
					burgerMain.frame.setVisible(true);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	
	}
	
	public Burger_Main()
	{
		initialize();
	}

	private void initialize()
	{
		frame = new JFrame();
		frame.setTitle("Game");
		frame.getContentPane().setBackground(new Color(188, 188, 188));
		frame.setBackground(new Color(0, 0, 0));
		frame.setSize(446, 700);
		frame.setLayout(null);	//기본으로 레이아웃 설정해주는 것을 null처리 해줘야 버튼 크기 자동 설정을 막을 수 잇다
		// 게임 시작 버튼 구현
		JButton buttonStart = new JButton("Game Start");
		buttonStart.setForeground(new Color(0, 0, 0));
		buttonStart.setBackground(new Color(135,206,235));
		buttonStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputButtonController)burgerMain).GameStartListener("1");
			}
		});
		buttonStart.setBounds(160,300,100,30);
		frame.getContentPane().add(buttonStart);
		
		//스코어 화면 버튼
		JButton buttonScore = new JButton("Game Score");
		buttonScore.setForeground(new Color(0, 0, 0));
		buttonScore.setBackground(new Color(135,206,235));
		buttonScore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputButtonController)burgerMain).GameStartListener("1");
				
			}
		});
		buttonScore.setBounds(160,350,100,30);
		frame.getContentPane().add(buttonScore);
		
		//게임 종료 버튼
		JButton buttonExit = new JButton("Exit");
		buttonExit.setForeground(new Color(0, 0, 0));
		buttonExit.setBackground(new Color(135,206,235));
		buttonExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputButtonController)burgerMain).GameStartListener("1");
			}
		});
		buttonExit.setBounds(160,400,100,30);
		frame.getContentPane().add(buttonExit);
	}
	
}

//스타트 버튼을 눌렀을 때 새로 나오는 창 정의
class newWindow extends JFrame{
	
	protected JFrame frameOne;
	
	public static Burger_Main burgerMain;
	
	List<Integer> questionList = new ArrayList<Integer>();
	
	ImageIcon[] images = {new ImageIcon("img/BurgerOne.png"),new ImageIcon("img/BurgerTwo.png"),
			new ImageIcon("img/BurgerThree.png"), new ImageIcon("img/BurgerFour.png"),
			new ImageIcon("img/BurgerFive.png"),new ImageIcon("img/BurgerSix.png")};
	
	newWindow()
	{
		frameOne = new JFrame();
		//프레임 틀을 만듬
		setTitle("Burger_Game");
		JPanel NewWindowContainer = new JPanel();
		setContentPane(NewWindowContainer);
		setLayout(null);
		//클릭하는 이미지 버튼1 생성
		JButton ImageOne = new JButton(images[0]);
		ImageOne.setForeground(new Color(0, 0, 0));
		ImageOne.setBackground(new Color(255,255,255));
		ImageOne.setBorderPainted(false);
		ImageOne.setFocusPainted(false);
		ImageOne.setContentAreaFilled(false);
		ImageOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputButtonController)burgerMain).GameStartListener("1");
			}
		});
		ImageOne.setBounds(20,80,180,100);
		add(ImageOne);
		//클릭하는 이미지 버튼2 생성
		JButton ImageTwo = new JButton(images[1]);
		ImageTwo.setForeground(new Color(0, 0, 0));
		ImageTwo.setBackground(new Color(255,255,255));
		ImageTwo.setBorderPainted(false);
		ImageTwo.setFocusPainted(false);
		ImageTwo.setContentAreaFilled(false);
		ImageTwo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputButtonController)burgerMain).GameStartListener("1");
			}
		});
		ImageTwo.setBounds(220,80,180,100);
		add(ImageTwo);
		//클릭하는 이미지 버튼3 생성
		JButton ImageThree = new JButton(images[2]);
		ImageThree.setForeground(new Color(0, 0, 0));
		ImageThree.setBackground(new Color(255,255,255));
		ImageThree.setBorderPainted(false);
		ImageThree.setFocusPainted(false);
		ImageThree.setContentAreaFilled(false);
		ImageThree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputButtonController)burgerMain).GameStartListener("1");
			}
		});
		ImageThree.setBounds(420,80,180,100);
		add(ImageThree);
		//클릭하는 이미지 버튼4 생성
		JButton ImageFour = new JButton(images[3]);
		ImageFour.setForeground(new Color(0, 0, 0));
		ImageFour.setBackground(new Color(255,255,255));
		ImageFour.setBorderPainted(false);
		ImageFour.setFocusPainted(false);
		ImageFour.setContentAreaFilled(false);
		ImageFour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputButtonController)burgerMain).GameStartListener("1");
			}
		});
		ImageFour.setBounds(620,80,180,100);
		add(ImageFour);
		//클릭하는 이미지 버튼5 생성
		JButton ImageFive = new JButton(images[4]);
		ImageFive.setForeground(new Color(0, 0, 0));
		ImageFive.setBackground(new Color(255,255,255));
		ImageFive.setBorderPainted(false);;
		ImageFive.setFocusPainted(false);
		ImageFive.setContentAreaFilled(false);;
		ImageFive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputButtonController)burgerMain).GameStartListener("1");
			}
		});
		ImageFive.setBounds(820,80,180,100);
		add(ImageFive);
		//클릭하는 이미지 버튼6 생성
		JButton ImageSix = new JButton(images[5]);
		ImageSix.setForeground(new Color(0, 0, 0));
		ImageSix.setBackground(new Color(255,255,255));
		ImageSix.setBorderPainted(false);
		ImageSix.setFocusPainted(false);
		ImageSix.setContentAreaFilled(false);
		ImageSix.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputButtonController)burgerMain).GameStartListener("1");
			}
		});
		ImageSix.setBounds(1020,80,180,100);
		add(ImageSix);
		//점수 표시하는 라벨 출력
		JLabel labelScore = new JLabel();
		labelScore.setBounds(550,10,100,40);
		labelScore.setText("0 점");
		getContentPane().add(labelScore);
		//문제 이미지 출력용 판넬 생성
		JPanel panelQuestion= new JPanel();
		panelQuestion.setBounds(30, 200, 550, 400);
		panelQuestion.setBackground(new Color(135,122,241));
		panelQuestion.setLayout(null);		// 레이아웃 사용없이 절대 위치값 적용
		add(panelQuestion);
		//정답 이미지 출력용 판넬 생성
		JPanel panelAnswer = new JPanel();
		panelAnswer.setBounds(620, 200, 550, 400);
		panelAnswer.setBackground(new Color(100,100,100));
		panelAnswer.setLayout(null);		//레이아웃 사용없이 절대 위치값 적용
		add(panelAnswer);
		
		
		
		panelQuestion=QuestionMethod(panelQuestion);
//		//문제용 이미지 라벨
//		JLabel ImageQuestionOne = new JLabel(images[0]);
//		ImageQuestionOne.setBounds(100,250,300,200);
//		panelQuestion.add(ImageQuestionOne);
//		
//		JLabel ImageQuestionTwo = new JLabel(images[1]);
//		ImageQuestionTwo.setBounds(100,230,300,200);
//		panelQuestion.add(ImageQuestionTwo);
//
//		
//		JLabel ImageQuestionThree = new JLabel(images[2]);
//		ImageQuestionThree.setBounds(100,210,300,200);
//		panelQuestion.add(ImageQuestionThree);
//
//		
//		JLabel ImageQuestionFour = new JLabel(images[3]);
//		ImageQuestionFour.setBounds(100,190,300,200);
//		panelQuestion.add(ImageQuestionFour);
//		
//		JLabel ImageQuestionFive = new JLabel(images[4]);
//		ImageQuestionFive.setBounds(100,170,300,200);
//		panelQuestion.add(ImageQuestionFive);
//		
//		JLabel ImageQuestionSix = new JLabel();
//		ImageQuestionSix.setIcon(images[5]);
//		ImageQuestionSix.setBounds(165,150,300,200);
//		panelQuestion.add(ImageQuestionSix);
//		
//		//Z축 순서 내가 원하는 대로 정렬하기
//		panelQuestion.setComponentZOrder(ImageQuestionSix, 0);
//		panelQuestion.setComponentZOrder(ImageQuestionFive, 1);
//		panelQuestion.setComponentZOrder(ImageQuestionFour, 2);
//		panelQuestion.setComponentZOrder(ImageQuestionThree, 3);
//		panelQuestion.setComponentZOrder(ImageQuestionTwo, 4);
//		panelQuestion.setComponentZOrder(ImageQuestionOne, 5);
		
		//프레임 사이즈 정의
		setSize(1300,700);
		setResizable(false);
		setVisible(true);
	}
	
	public JPanel QuestionMethod(JPanel panel)
	{
		JPanel newpanel = new JPanel();
		Random random = new Random();
		int lengthnum = random.nextInt(4)+3; //3~6까지의 랜덤 int 출력
		JLabel ImageQuestionOne = new JLabel();
		JLabel ImageQuestionTwo = new JLabel();
		JLabel ImageQuestionThree = new JLabel();
		JLabel ImageQuestionFour = new JLabel();
		JLabel ImageQuestionFive = new JLabel();
		JLabel ImageQuestionSix = new JLabel();
		//for문을 사용하기 위해 변수명을 hashmap으로 생성하기
		HashMap<Integer,JLabel> values = new HashMap<Integer,JLabel>();
		values.put(0, ImageQuestionOne);
		values.put(1, ImageQuestionTwo);
		values.put(2, ImageQuestionThree);
		values.put(3, ImageQuestionFour);
		values.put(4, ImageQuestionFive);
		values.put(5, ImageQuestionSix);
		int x= 250;
		//길이에 따라서 다르게 리스트 값을 설정하는 부분
		for(int order=0;order<lengthnum;order++) {
			if(order==0) {
				//0번째 자리에 밑에 빵 넣기
				values.get(order).setIcon(images[order]);
			}
			else if(order==lengthnum-1) {
				//마지막 자리에 위에 빵 넣기
				values.get(order).setIcon(images[5]);
			}
			//그 외의 순서에는 그냥 랜덤으로 숫자 넣기
			else {
				int randnum = QuestionRandnum();
				values.get(order).setIcon(images[randnum]);
			}
			//라벨의 위치값 지정하기
			values.get(order).setBounds(165, x, 300, 200);
			x=x-20;
			newpanel.add(values.get(order));
		}
		int location = 0;
		// z-index 위치 지정하기
		for(int repeat = lengthnum;repeat>-1;repeat--) {
			newpanel.setComponentZOrder(values.get(repeat), location);
			location++;
		}
		newpanel.setBounds(0, 0, 550, 400);
		newpanel.setLayout(null);
		panel.add(newpanel);
		return panel;
	}
	
	public int QuestionRandnum()
	{
		Random randomQuestion = new Random();
		return randomQuestion.nextInt(4)+1;//1~4까지 랜덤 int 출력
	}

}
